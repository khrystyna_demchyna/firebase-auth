import cookieParser from "cookie-parser";
import csrf from "csurf";
import bodyParser from "body-parser";
import express from "express";
import admin from "./config/firebase-config.js";
import Middleware from "./middleware/verify-token.js";
import ejs from "ejs";

const csrfMiddleware = csrf({ cookie: true });
const middleware = new Middleware();
const PORT = 4000;
const app = express();

app.engine("html", ejs.renderFile);
app.use(express.static("static"));

app.use(bodyParser.json());
app.use(cookieParser());
app.use(csrfMiddleware);

app.all("*", (req, res, next) => {
  res.cookie("XSRF-TOKEN", req.csrfToken());
  next();
});

app.get("/login", function (req, res) {
  res.render("login.html");
});

app.get("/profile", function (req, res) {
  const sessionCookie = req.cookies.session || "";

  admin
    .auth()
    .verifySessionCookie(sessionCookie, true)
    .then((userData) => {
      console.log("Logged in:", userData.email);
      res.render("profile.html");
    })
    .catch((error) => {
      res.redirect("/login");
    });
});

app.post("/sessionLogin", middleware.decodeToken, (req, res) => {
  const idToken = req.headers.authorization.split(' ')[1];
  const expiresIn = 60 * 60 * 24 * 5 * 1000;

  admin
    .auth()
    .createSessionCookie(idToken, { expiresIn })
    .then(
      (sessionCookie) => {
        const options = { maxAge: expiresIn, httpOnly: true };
        res.cookie("session", sessionCookie, options);
        res.end(JSON.stringify({ status: "success" }));
      },
      (error) => {
        res.status(401).send("UNAUTHORIZED REQUEST!");
      }
    );
});

app.get("/sessionLogout", (req, res) => {
  res.clearCookie("session");
  res.redirect("/login");
});

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`);
});
