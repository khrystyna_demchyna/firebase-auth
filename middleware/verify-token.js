import admin from "../config/firebase-config.js";

class Middleware {
  async decodeToken(req, res, next) {
    const token = req.headers.authorization.split(" ")[1];

    if (!token) {
      return response.status(401).send({ message: "No token provided" });
    }
    try {
      const decodeValue = await admin.auth().verifyIdToken(token);
      if (decodeValue) {
        req.token = decodeValue;
        return next();
      }
      return res.json({ message: "Unauthorize" });
    } catch (e) {
      return res.json({ message: "Internal Error" });
    }
  }
}
export default Middleware;
