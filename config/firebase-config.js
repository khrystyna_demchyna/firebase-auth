import admin from "firebase-admin";
import serviceAccountKey from "./serviceAccountKey.js"

admin.initializeApp({
  credential: admin.credential.cert(serviceAccountKey),
  databaseURL: "https://friendlychat-1dcbe-default-rtdb.firebaseio.com",
});

export default admin;
